'use strict';

const express = require('express');
const randomstring = require("randomstring");

// Constants
const PORT = 8080;

// App
const app = express();
const seed = randomstring.generate();

app.get('/', function (req, res) {
  res.send('Hello from nodejs microservice. Seed: \n' + seed);
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
